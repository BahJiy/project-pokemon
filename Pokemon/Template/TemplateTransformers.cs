﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Pokemon.Template {
	public interface ITransformer {
		List<byte> Transform ( List<byte> data, byte inter = 0x0 );
		byte Range ( );
	}

	public abstract class ITransByte : ITransformer {
		/// <summary>
		/// This method contains the logic as to how to pass the file data to the
		/// transform method. It is automatically called
		/// </summary>
		/// <param name="data">The data to be transform</param>
		/// <param name="inter">The current interation if applicable</param>
		/// <returns>The transformed data</returns>
		List<byte> ITransformer.Transform ( List<byte> data, byte inter ) {
			data.ForEach ( x => transformByte ( x, inter ) );
			return data;
		}

		/// <summary>
		/// Provides the logic for the transformation
		/// </summary>
		/// <param name="data">The byte to transform</param>
		/// <param name="inter">The current byte interation give by Range()</param>
		/// <returns>The transformed byte</returns>
		public abstract byte transformByte ( byte data, byte inter );

		/// <summary>
		/// /// All iterations possible for this transformation
		/// </summary>
		/// <returns>Yield a byte</returns>
		public abstract byte Range ( );
	}

	public abstract class ITransArray : ITransformer {
		/// <summary>
		/// This method contains the logic as to how to pass the file data to the
		/// transform method. It is automatically called
		/// </summary>
		/// <param name="data">The data to be transform</param>
		/// <param name="inter">The current interation if applicable</param>
		/// <returns>The transformed data</returns>
		List<byte> ITransformer.Transform ( List<byte> data, byte inter ) =>
			transformArray ( data );

		/// <summary>
		/// Provides the logic for the transformation
		/// </summary>
		/// <param name="data">The byte array to transform</param>
		/// <returns>The array of transformed bytes</returns>
		public abstract List<byte> transformArray ( List<byte> data );

		/// <summary>
		/// Not supported in ITransArray
		/// </summary>
		byte ITransformer.Range ( ) => 0x0;

	}
}
