﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

using Pokemon.Template;
namespace Pokemon {
	public struct ProcessInfo {

		public List<ITransformer> transformer { get; set; }
		public int? level { get { return level ?? 1; } set { level = value; } }
		public int? trans1KeepNum {
			get { return trans1KeepNum ?? 20; }
			set { trans1KeepNum = value; }
		}
		public int? trans2KeepNum {
			get { return trans2KeepNum ?? 20; }
			set { trans2KeepNum = value; }
		}
		public bool? zipFile { get { return zipFile ?? false; } set { zipFile = value; } }
		public bool? saveFile { get { return saveFile ?? false; } set { saveFile = value; } }
		public bool? verbose { get { return verbose ?? false; } set { verbose = value; } }

		public string outputDir {
			get { return outputDir == "" ? "output" : outputDir; }
			set { outputDir = value; }
		}
		public string transName { get; set; }
	}

	public class Balbuzard {
		private Stream stream { get; set; }
		private ProcessInfo info;


		private Balbuzard ( Stream stream, ProcessInfo info ) {
			this.stream = stream;
			this.info = info;
		}

		/// <summary>
		/// Create an instance of Balbuzard with a file from a zip
		/// </summary>
		/// <param name="zipFile">Path to the zip file</param>
		/// <returns>An instance of Balbuzard</returns>
		public Balbuzard ReadZip ( string zipFile, ProcessInfo info ) {
			List<ZipArchiveEntry> filesList;
			int choice;

			Console.WriteLine ( "Choose a file to evaluate in the zip:" );
			using ( ZipArchive zip = ZipFile.OpenRead ( zipFile ) ) {
				// Read the file list in the zip
				filesList = new List<ZipArchiveEntry> ( zip.Entries.Count );
				for ( int i = 0; i < zip.Entries.Count; i++ ) {
					Console.WriteLine ( "{0}: {1}", i + 1, zip.Entries[ i ].FullName );
					filesList.Add ( zip.Entries[ i ] );
				}

			QUESTION:
				Console.WriteLine ( "Which file do you want to evaluate (1 - {0})", filesList.Count );
				try {
					// Parse the user's answer to which file to evaluate
					string answer = Console.ReadLine ( );
					if ( answer.Equals ( "q" ) )
						return null;
					choice = int.Parse ( answer );
					// If the choice is out of range, ask to try again
					if ( choice < 0 || choice > filesList.Count )
						throw new FormatException ( );
				} catch ( FormatException ) {
					Console.WriteLine ( "Invalid choice. Type \"q\" to cancel..." );
					goto QUESTION;
				}

				return new Balbuzard ( filesList[ choice - 1 ].Open ( ), info );
			}
		}

		/// <summary>
		/// Create an instance of Balbuzard with a file
		/// </summary>
		/// <param name="file">Path to the file</param>
		/// <returns>An instance of Balbuzard</returns>
		public Balbuzard ReadFile ( string file, ProcessInfo info ) =>
			new Balbuzard ( File.Open ( file, FileMode.Open ), info );
	}
}
